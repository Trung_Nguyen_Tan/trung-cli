import fs from "fs";
import { constantCase } from "constant-case";

export const generateGraphqlFile = ({
  apiName,
  path,
  mutation,
  isGetRequiredField,
  ...config
}) => {
  // ex: getAvailableSORUnitInSORItem(ProjectID: ID): [SORUnit!]!

  if (!config.schemaPath) {
    return console.log("Please config the schemaPath in the config file");
  }

  var content = fs.readFileSync(config.schemaPath, "utf8");

  String.prototype.isReturnObject = function () {
    return this.charAt(0) === this.charAt(0).toLowerCase();
  };

  String.prototype.isRequiredField = function () {
    return this.charAt(this.length - 1) === "!";
  };

  String.prototype.clearSymbol = function () {
    return this.replace(/[^a-zA-Z0-9]/g, "");
  };

  // the string after api name ex: (ProjectID: ID): [SORUnit!]!
  if (!content.split(apiName)[1]) {
    return console.log(`API ${apiName} not found`);
  }
  const temp = content
    .split(apiName)[1]
    .slice(0, content.split(apiName)[1].indexOf("\n"));
  let params = "";
  let convertParam1 = "";
  let convertParam2 = "";
  if (temp.charAt(0) === "(") {
    params = temp.slice(1, temp.lastIndexOf(")"));
    convertParam1 = `(${params
      .split(", ")
      .map((e) => `$${e}`)
      .join(", ")})`;
    convertParam2 = `(${params
      .split(", ")
      .map((e) => {
        const keyName = e.split(":")[0];
        return `${keyName}: $${keyName}`;
      })
      .join(", ")})`;
  } else {
  }

  // return type ex: SORUnit
  const typeName = temp
    .slice(temp.lastIndexOf(": "), temp.length)
    .clearSymbol();

  const handleMapType = (type, stop) => {
    const checkFoundType = content.split(`type ${type}`)[1];
    if (checkFoundType) {
      const rawType = checkFoundType
        .slice(2, content.split(`type ${type}`)[1].indexOf("}"))
        .trim();
      let filterType = rawType.split("\n");
      if (isGetRequiredField)
        filterType = rawType
          .split("\n")
          .filter((e) => e.trim().isRequiredField());
      const convertType = filterType
        .map((e) =>
          !stop && e.trim().isReturnObject()
            ? `${e.slice(0, e.indexOf(":"))} ${handleMapType(
                e.split(": ")[1].clearSymbol(),
                true
              )}`
            : e.slice(0, e.indexOf(":"))
        )
        .join(`\n`);
      return `{${convertType}}`;
    } else {
      return "";
    }
  };

  const graphqlContent = `${
    mutation ? "mutation" : "query"
  } ${apiName}${convertParam1} {
  ${apiName}${convertParam2} ${handleMapType(typeName)}
}
`;

  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }

  //export const GET_PAGING_SUPPLIERS = loader('./getPagingSuppliers.graphql')

  const constant = `\nexport const ${constantCase(
    apiName
  )} = loader('./${apiName}.graphql')`;

  if (fs.existsSync(path + "/index.ts")) {
    fs.appendFile(path + "/index.ts", constant);
  } else if (fs.existsSync(path + "/graphql.ts")) {
    fs.appendFile(path + "/graphql.ts", constant);
  } else {
    fs.writeFileSync(`.constant.txt`, constant);
  }

  fs.writeFileSync(`${path}/${apiName}.graphql`, graphqlContent);
  console.log(
    `Create file successfully. The file locate in ${path}/${apiName}.graphql`
  );
};
