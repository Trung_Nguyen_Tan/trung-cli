#!/usr/bin/env node

import Yargs from "yargs";
import { findUp } from "find-up";
import fs from "fs";
import { generateGraphqlFile } from "./gen-graphql-file.js";

const configPath = await findUp([".trungclirc.js", ".trungclirc.json"]);
const config = configPath ? JSON.parse(fs.readFileSync(configPath)) : {};

const yards = Yargs(process.argv.slice(2)).config(config);

yards
  .command(
    "create-graphql-file <apiName>",
    "create graphql query|mutation file  base on API name",
    (yargs) => {
      return yargs
        .option("mutation", {
          describe: "mutation graphql file",
          alias: "-m",
          boolean: true,
        })
        .option("required", {
          describe: "get only get required fields",
          alias: "-r",
          boolean: true,
        })
        .option("path", {
          describe: "destination path of graphl file",
          alias: "-p",
          default: "./graphql",
        });
    },
    (argv) => generateGraphqlFile(argv)
  )
  .help().argv;
